package com.classpath.kafka.producer.client;

import com.classpath.kafka.producer.config.AppConfig;
import com.github.javafaker.Faker;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.IntegerSerializer;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.Properties;

public class ProducerDemo {
    public static void main(String[] args) throws InterruptedException {
        Properties props = new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, AppConfig.BOOTSTRAP_SERVERS);
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, IntegerSerializer.class.getName());
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        KafkaProducer<Integer, String> producer = new KafkaProducer<>(props);
        Faker faker = new Faker();
        for (int index = 0; index < 1000; index++) {
            String fact = faker.chuckNorris().fact();
            ProducerRecord<Integer, String> producerRecord = new ProducerRecord<>("day-1", fact);
            Thread.sleep(2000);
            producer.send(producerRecord);
            System.out.println("Sending message" + fact);
        }
        producer.close();
    }
}
